# Ronny CV Admin

**RonnyCVAdmin** is a project that help you build up a web app that can generate curriculum vitae pages from your input information and it also can export into PDF files.
The project was build using **Symfony 2.8** which you can find its document [here](https://symfony.com/doc/current/index.html).

## Prerequisites

Before install this project, you need to have Composer installed in your machine. To install composer, follow the instruction [here](https://getcomposer.org/doc/00-intro.md).

## Installation

1. Clone the project
```
git clone https://ronnyvo@bitbucket.org/ronnyvo/ronnycvadmin.git
cd ronnycvadmin
```

1. Install dependencies
```
composer install
```
While installation, the installer needs you to input your database information, you can input at that time or just enter to input the default information then edit it later at **app/config/parameters.yml**

1. Update database
```
php app/console doctrine:schema:update --force
```

1. Running your application
```
php app/console server:run
```
Navigate to `http://localhost:8000/` to view the project at development mode.