<?php

namespace Ronny\CVAdminBundle\Controller;

use Ronny\CVAdminBundle\Entity\CurriculumVitae;
use Ronny\CVAdminBundle\Form\CurriculumVitaeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CVController extends Controller
{
    public function indexAction(Request $request){
        $cv = new CurriculumVitae();
        $form = $this->createForm(new CurriculumVitaeType(), $cv);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $cv->setStatus(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cv);
            $em->flush();

            $this->addFlash(
                'cv-success',
                'Your CV has been created successfully!'
            );

            return $this->redirectToRoute('ronny_cv_admin_cv_success', array(
                'id' => $cv->getId()
            ));
        }

        return $this->render('RonnyCVAdminBundle:CV:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function successAction($id) {
        $flashBag = $this->get('session')->getFlashBag();
        $success = $flashBag->get('cv-success');
        $allowAccess = true;
        if (!$success && !$allowAccess) {
            return $this->redirectToRoute('ronny_cv_admin_cv');
        }
        $this->addFlash(
            'cv-success',
            'Your CV has been created successfully!'
        );
        return $this->render('RonnyCVAdminBundle:CV:success.html.twig', array(
            'cvID' => $id
        ));

    }

    public function renderAction($id, Request $request) {

        $repository = $this->getDoctrine()
            ->getRepository('RonnyCVAdminBundle:CurriculumVitae');

        $cv = $repository->find($id);
        if (!$cv) {
            throw $this->createNotFoundException(
                'No data found for id '.$cv
            );
        }

        $fileName = preg_replace('/\s+/', '', $cv->getUserName()).'_CV.pdf';

        $response = $this->forward('RonnyCVAdminBundle:CV:view', array(
            'id'  => $id,
            'from_render' => true,
        ));
        $html = $response->getContent();
        
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="'.$fileName.'"'
            )
        );
    }

    public function viewAction($id, Request $request, $from_render = false) {

        $repository = $this->getDoctrine()
            ->getRepository('RonnyCVAdminBundle:CurriculumVitae');

        $cv = $repository->find($id);
        if (!$cv) {
            throw $this->createNotFoundException(
                'No data found for id '.$cv
            );
        }

        $link_absolute = false;
        if ($from_render) {
            $link_absolute = true;
        }

        return $this->render('RonnyCVAdminBundle:CV/Render:view.html.twig', array(
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath(),
            'upload_uri_prefix' => $this->container->getParameter('upload_uri_prefix'),
            'data' => array(
                'info' => array(
                    'user_name' => $cv->getUserName(),
                    'date_of_birth' => $cv->getDateOfBirth(),
                    'phone' => $cv->getPhone(),
                    'email' => $cv->getEmail(),
                    'address' => $cv->getAddress(),
                    'user_picture' => $cv->getUserPicture()
                )
            ),
            'link_absolute' => $link_absolute
        ));
    }
}
