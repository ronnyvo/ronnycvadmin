<?php

namespace Ronny\CVAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RonnyCVAdminBundle:Default:index.html.twig');
    }
}
