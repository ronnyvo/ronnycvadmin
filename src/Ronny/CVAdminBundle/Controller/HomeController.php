<?php

namespace Ronny\CVAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction() {
        return $this->render('RonnyCVAdminBundle:Home:index.html.twig');
    }

    public function aboutAction() {
        return $this->render('RonnyCVAdminBundle:Home:about.html.twig');
    }

    public function friendAction() {
        return $this->render('RonnyCVAdminBundle:Home:friend.html.twig');
    }
}
