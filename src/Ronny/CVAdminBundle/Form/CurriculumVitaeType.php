<?php

namespace Ronny\CVAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CurriculumVitaeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('userName')
            ->add('userPictureFile', new FileType(), array('label' => 'Upload your picture'))
            ->add('dateOfBirth', 'date', array(
                'years' => range(date('Y') -30, date('Y'))
            ))
            ->add('phone')
            ->add('email')
            ->add('address')
            ->add('shortDescription')
            ->add('objective')
            //->add('status')
            //->add('createdAt', 'datetime')
            //->add('updatedAt', 'datetime')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ronny\CVAdminBundle\Entity\CurriculumVitae'
        ));
    }
}
